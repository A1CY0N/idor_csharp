# IDOR_CSharp

> :warning: **If you are using this project**: This project was created mainly to learn C# and how RSA work. It is not a safe implementation to use in other projects. **It should only be used for learning or testing purposes.**

## Description

ASP.NET app to illustrate IDOR vulnerability

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/idor_csharp>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request